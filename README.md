# Blender Insel

## Sammlung aller Teilprojektdateien

## Nützliche Links
### Tutorials
- Sea-Shack-Modeling and Texturing: https://www.youtube.com/watch?v=Jmir2bDpZA4
- Well Modeling and Texturing: https://www.youtube.com/watch?v=OlnkGCdtGEw
- Sand Procedural Material: https://www.youtube.com/watch?v=je8C03Yoc8A
- Basics of Material/Texture Nodes: https://www.youtube.com/watch?v=yffWd4kI51Q
- Beginners Guide for simple animations: https://www.youtube.com/watch?v=zp6kCe5Kmf4

### Misc
- Free Textures: https://www.poliigon.com/textures/free/
